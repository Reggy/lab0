﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Woda : Napoj
    {
        public string nazwaUjscia { get; set; }
        public bool czyPusta { get; set; }

        private int iloscKalorii { get; set; }

        public int IloscKalorii
        {
            get
            {
                return iloscKalorii;
            }
        }

        public Woda():base()
        {
 
        }

        public Woda(int pojemnosc, bool czySmak, string marka, string nazwaUjscia, bool czyPusta, int iloscKalorii)
            : base(pojemnosc, czySmak, marka)
        {
            this.iloscKalorii = iloscKalorii;
            this.nazwaUjscia = nazwaUjscia;
            this.czyPusta = czyPusta;
        }

        public override string Nazwa()
        {
            return "woda";
        }


        public bool CzyPustaButelka()
        {
            return czyPusta;
        }
    }
}
