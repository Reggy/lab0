﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Sok : Napoj
    {
        public string smak { get; set; }
        public string opakowanie { get; set; }

        public Sok():base()
        {

        }

        public Sok(int pojemnosc, string marka, bool czySmak, string smak, string opakowanie)
            : base(pojemnosc, czySmak, marka)
        {
            this.smak = smak;
            this.opakowanie = opakowanie;
        }

        public override string Nazwa()
        {
            return "sok";
        }

        public string ZwrocOpakowanie()
        {
            return opakowanie;
        }
    }
}
