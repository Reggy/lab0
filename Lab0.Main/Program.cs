﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            Woda w = new Woda(2, false, "Dobrowianka", "Ujscie", false, 100);
            Sok s = new Sok(1, "Costa", true, "Pomarańcz", "Karton");

            //Dziedziczenie 
            s.Nazwa();
            w.Nazwa();

            //Hermetyzacja 
            Console.WriteLine(w.IloscKalorii);


            //Polimorfizm 
            Napoj[] napoje = new Napoj[] { w, s };

            for (int i = 0; i < napoje.Length; i++)
            {
                Console.WriteLine(napoje[i].marka);
            }

            Console.ReadKey();
        }
    }
}
