﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Napoj
    {
        public int pojemnosc { get; set; }
        public bool czySmak { get; set; }
        public string marka { get; set; }

        public Napoj()
        { 

        }

        public Napoj(int pojemnosc, bool czySmak, string marka)
        {
            this.pojemnosc = pojemnosc;
            this.czySmak = czySmak;
            this.marka = marka;
        }

        public virtual string Nazwa()
        {
            return "napoj";
        }
        public bool CzySmakowy()
        {
            return czySmak;
        }

    }
}
